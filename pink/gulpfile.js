"use strict";

var gulp = require("gulp");
var jade = require('gulp-jade');
var less = require("gulp-less");
var svgstore = require("gulp-svgstore");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var server = require("browser-sync");

gulp.task("icons", function () {
  return gulp
    .src("./img/*.svg")
    .pipe(svgstore({
      inlineSvg: true
    }))
    .pipe(gulp.dest("dist/assets"));
});

gulp.task("img", function () {
  return gulp
    .src("./img/*")
    .pipe(gulp.dest("dist/img"))
});

gulp.task("fonts", function () {
  return gulp
    .src("./fonts/*")
    .pipe(gulp.dest("dist/fonts"))
});

gulp.task("style", function() {
  gulp.src("less/style.less")
    .pipe(plumber())
    .pipe(less())
    .pipe(postcss([
      autoprefixer({browsers: [
        "last 1 version",
        "last 2 Chrome versions",
        "last 2 Firefox versions",
        "last 2 Opera versions",
        "last 2 Edge versions"
      ]})
    ]))
    .pipe(gulp.dest("dist/css"))
    .pipe(server.reload({ stream: true }));
});

gulp.task("jade", ["icons"], function() {
  gulp.src([ "jade/**/*.jade", "!jade/includes/*.jade" ], { base: "jade" })
    .pipe(jade())
    .pipe(gulp.dest("dist/"))
    .pipe(server.reload({ stream: true }))
});

gulp.task("default", [ "img", "fonts", "icons", "style", "jade" ]);

gulp.task("serve", [ "img", "fonts", "icons", "style", "jade" ], function() {
  server.init({
    server: "dist",
    notify: false,
    open: true,
    port: 3001,
    ui: false
  });

  gulp.watch("less/**/*.less", [ "style" ], server.reload);
  gulp.watch("jade/**/*.jade", [ "jade" ], server.reload);
});
